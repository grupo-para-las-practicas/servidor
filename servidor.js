const express = require('express');
const app = express();
const puerto = 8032;

app.get('/', (req, res)=>{
  res.send("<html><h2>¡Hola mundo!</h2></html>");
  visitas++;
});

app.get('/ruta1', (req, res)=>{
  res.send("<html><h3>¡Esta es la ruta 1!</h3></html>");
  visitas++;
});

app.get('/ruta2', (req, res)=>{
  res.send("<html><h3>¡Esta es la ruta 2!</h3></html>");
  visitas++;
});

app.get('/ruta3', (req, res)=>{
  res.send("<html><h3>¡Esta es la ruta 3!</h3></html>");
  visitas++;
});

app.get('/ruta4', (req, res)=>{
  res.send("<html><h3>¡Esta es la ruta 4!</h3></html>");
  visitas++;
});

//puedo realizar el cambio correctamente
app.get('/rutaultima', (req, res)=>{
  res.send("<html><h3>¡Esta es la utima ruta!</h3></html>");
  visitas++;
});

app.listen(puerto, function() {
  console.log("El servidor está activo en http://localhost:8032/");
});